$("#datestart").datepicker();
$("#dateend").datepicker();
$("#dateend1").datepicker();
$("#imageicon").on('change',function (e) {

    var file=e.target.files[0];
    var reader=new FileReader();
    if (file && file.type.match('image.*')) {
        reader.readAsDataURL(file);
    } else {
        $(".preview-icon").html("Image không được chọn hoặc image không đúng định dạng");
    }
    reader.onload=function (ev) {
        var temp='<img src="'+ev.target.result+'" alt="" width="80px">';
        $(".preview-icon").html(temp);
    }
});


$("#imageproduct").on('change',function (e) {

    var files=e.target.files;
    filesLength = files.length;
    for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
            var file = e.target;
            var temp='<span class="list"><img src="'+e.target.result+'" alt="" width="80px"><span class="remove"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></span></span>';
            $(".preview-image").append(temp);

            $(".remove").click(function(){
                $(this).parent(".list").remove();
                var file = $(this).parent();
                console.log(file);
                // for(var i = 0; i < filesLength.length; i++) {
                //     if(filesLength[i].name == file) {
                //         filesLength.splice(i, 1);
                //         break;
                //     }
                // }
            });

        });
        fileReader.readAsDataURL(f);
    }
});

$("#modal_add_address").click(function () {
    $("#modaladdaddress").modal("show");
});
$("#modal_add").click(function () {
    $('#myModal').modal("show");
});

$("#button_admin").click(function () {
    $('#myModal').modal("show");
});

$("#addrole").click(function (e) {
    e.preventDefault();
    $("#modaladdrole").modal('show');
});

$("#addpermission").click(function (e) {
    e.preventDefault();
    $("#modaladdper").modal("show");
});

$("#modaladdpro").click(function (e) {
   e.preventDefault();
   $("#modaladdproduct").modal('show');
});

$(document).on("click",".btn-edit-admin",function (e) {
   e.preventDefault();
   $("#modaleditadmin").modal("show");
   var id=$(this).val();
    $.ajax({
        url:"/admin/user/edit-admin",
        method:"GET",
        data:{id:id},
        dataType:"json",
    }).done(function (data) {
       $("#name").val(data.name);
       $("#txtemail").val(data.email);
       if(data.role==1){
           var select="<select name=\"role\" class=\"form-control\">";
           select+="<option value=\"1\" selected>admin</option>\n" +
               "<option value=\"2\">staff</option>\n" +
               " <option value=\"3\">publicer</option>";
           select+=" </select>";
           $("#role").html(select);
       }
       else if(data.role==2){
           var select="<select name=\"role\" class=\"form-control\">";
           select+="<option value=\"1\" >admin</option>\n" +
               "<option value=\"2\" selected>staff</option>\n" +
               " <option value=\"3\">publicer</option>";
           select+=" </select>";
           $("#role").html(select);
       }
       else{
           var select="<select name=\"role\" class=\"form-control\">";
           select+="<option value=\"1\" >admin</option>\n" +
               "<option value=\"2\">staff</option>\n" +
               " <option value=\"3\" selected>publicer</option>";
           select+=" </select>";
           $("#role").html(select);
       }

        if(data.status==1){
            var select="<select name=\"status\" class=\"form-control\">";
            select+="<option value=\"1\" selected>Hoạt động</option>\n" +
                "<option value=\"2\">Không hoạt động</option>\n";

            select+=" </select>";
            $("#status").html(select);
        }
        else{
            var select="<select name=\"status\" class=\"form-control\">";
            select+="<option value=\"1\" >Hoạt động</option>\n" +
                "<option value=\"2\" selected>Không hoạt động</option>\n";

            select+=" </select>";
            $("#status").html(select);
        }
        $("#ideditadmin").val(data.id);
    }).fail(function (error) {
        console.log(error);
    });
});


// edit role

$(document).on("click",".btn-edit-role",function (e) {
    e.preventDefault();
    $("#modaleditrole").modal('show');
    var idrole=$(this).val();
    $.ajax({
       url:"/admin/role/edit-role",
       type:"GET",
        data:{id:idrole},
       dataType:"json"
    }).done(function (data) {

        $("#name").val(data.role.name);
        $("#idrole").val(data.role.id);

        var ck1="<div class=\"col-md-3\">";
            ck1+=" <p>Cate</p>";
        $.each(data.per,function (i,item) {
                if(item.permission_for=="cate"){
                    var flag=0;
                    $.each(data.rp,function (j,value) {
                        if(item.id==value.id){
                         ck1+="<label><input type=\"checkbox\" checked='checked' value="+value.id+" name=\"per_role[]\">"+value.name+"</label>";
                         flag=1;
                        }
                    });
                    if(flag==0){
                        ck1+="<label><input type=\"checkbox\" value="+item.id+" name=\"per_role[]\">"+item.name+"</label>";
                    }
                }

            // return false;
        });
        ck1+="</div>";

        var ck2="<div class=\"col-md-3\">";
        ck2+=" <p>User</p>";
        $.each(data.per,function (i,item) {
            if(item.permission_for=="user"){
                var flag=0;
                $.each(data.rp,function (j,value) {
                    if(item.id==value.id){
                        flag=1;
                        ck2+="<label><input type=\"checkbox\" checked='checked' value="+item.id+" name=\"per_role[]\">"+item.name+"</label>";

                    }
                });
                if(flag==0){
                    ck2+="<label><input type=\"checkbox\" value="+item.id+" name=\"per_role[]\">"+item.name+"</label>";
                }
            }
        });
        ck2+="</div>";

        var ck3="<div class=\"col-md-3\">";
        ck3+=" <p>Product</p>";
        $.each(data.per,function (i,item) {
            if(item.permission_for=="product"){
                var flag=0;
                $.each(data.rp,function (j,value) {
                    if(item.id==value.id){
                        ck3+="<label><input type=\"checkbox\" checked='checked' value="+item.id+" name=\"per_role[]\">"+item.name+"</label>";
                        flag=1;
                    }
                });
                if(flag==0){
                    ck3+="<label><input type=\"checkbox\" value="+item.id+" name=\"per_role[]\">"+item.name+"</label>";
                }
            }
        });
        ck3+="</div>";

        var ck=ck1+ck2+ck3;
        $("#checkbox").html(ck);

    });
});

// add admin

function addAdmin() {
    var form_data = new FormData($("#addadmin")[0]);
    $.ajax({
        url: '/admin/user/add-admin',
        type: 'POST',
        data: form_data,
        dataType: 'html',
        // async: false,
        processData: false,
        contentType: false,
    }).done(function (data) {
        $("#addadmin").trigger('reset');
        $('#myModal').modal('hide');
        $(".content-table").html(data);

    }).fail(function (data) {
        console.log(data);
        // var response = JSON.parse(data.responseText);
        // printErrorMsg(response.errors);
    });

}

//sửa admin
$("#editadmin").change(
    function () {
        $(".btneditadmin").removeAttr('disabled');
    }
);

function editAdmin() {
    var form_data = new FormData($("#editadmin")[0]);
    $.ajax({
        url: '/admin/user/edit-admin',
        type: 'POST',
        data: form_data,
        dataType: 'html',
        // async: false,
        processData: false,
        contentType: false,
    }).done(function (data) {
        $("#editadmin").trigger('reset');
        $("#modaleditadmin").modal('hide');
        $(".content-table").html(data);

    }).fail(function (data) {
        console.log(data);
        // var response = JSON.parse(data.responseText);
        // printErrorMsg(response.errors);
    });
}

// del admin
$(document).on('click','.btn-del-admin',function (e) {
    e.preventDefault();
    id=$(this).val();
    var check=confirm("Bạn có muốn xóa hay không ?");
    if(check==true){
        $.ajax({
            url: '/admin/user/del-admin/',
            type: 'GET',
            data: {id:id},
            dataType: 'html',
            // async: false,
            // processData: false,
            // contentType: false,
        }).done(function (data) {
            // $("#formeditcate").trigger('reset');
            // $('#myModalupdate').modal('hide');
            alert("Xóa thành công");
            $(".content-table").html(data);
            // page=$(".idpaginate").val();
            // readCate(page);

        }).fail(function (data) {
            var response = JSON.parse(data.responseText);
            console.log(response.errors);
        });
    }
    return false;
});


//add role

function addrole() {
    var data=$("#formaddrole").serialize();
    // console.log(data);
    $.ajax({
        url:"/admin/role/add-role",
        type:'POST',
        data:data,
        dataType:'html'
    }).done(function (data) {
        $("#formaddrole").trigger('reset');
        $('#modaladdrole').modal('hide');
        $(".content-table").html(data);
    }).fail(function (error) {
        console.log(error);
    });
}

// edit role
$("#formeditrole").change(
    function () {
        $(".btneditrole").removeAttr('disabled');
    }
);

function editrole() {
    var data=$("#formeditrole").serialize();
    // console.log(data);
    $.ajax({
        url:"/admin/role/edit-role",
        type:'POST',
        data:data,
        dataType:'html'
    }).done(function (data) {
        $("#formeditrole").trigger('reset');
        $('#modaleditrole').modal('hide');
        $(".content-table").html(data);
    }).fail(function (error) {
        console.log(error);
    });
}

//del role

$(document).on('click','.btn-del-role',function (e) {
    e.preventDefault();
    id=$(this).val();
    var check=confirm("Bạn có muốn xóa hay không ?");
    if(check==true){
        $.ajax({
            url: '/admin/role/del-role/',
            type: 'GET',
            data: {id:id},
            dataType: 'html',
            // async: false,
            // processData: false,
            // contentType: false,
        }).done(function (data) {
            // $("#formeditcate").trigger('reset');
            // $('#myModalupdate').modal('hide');
            alert("Xóa thành công");
            $(".content-table").html(data);
            // page=$(".idpaginate").val();
            // readCate(page);

        }).fail(function (data) {
            var response = JSON.parse(data.responseText);
            console.log(response.errors);
        });
    }
    return false;
});

// add permission

function addpermission() {
    var data=$("#addper").serialize();
    // console.log(data);
    $.ajax({
        url:"/admin/permission/add-permission",
        type:'POST',
        data:data,
        dataType:'html'
    }).done(function (data) {
        $("#addper").trigger('reset');
        $('#modaladdper').modal('hide');
        $(".content-table").html(data);
    }).fail(function (error) {
        console.log(error);
    });
}

//edit permission

$(document).on('click','.btn-edit-per',function (e) {
    e.preventDefault();
    $("#modaleditper").modal('show');
    var id=$(this).val();
    // alert($(this).val());
    $.ajax({
        url:"/admin/permission/edit-permission",
        type:'GET',
        data:{id:id},
        dataType:'json'
    }).done(function (data) {
        $("#name").val(data.name);
        $("#name_for").val(data.permission_for);
        $("#id_per").val(data.id);
    }).fail(function (error) {
        console.log(error);
    });

});

$("#editpermis").change(
    function () {
        $(".btn-edit-pers").removeAttr('disabled');
    }
);

function editpermission() {
    var data=$("#editpermis").serialize();
    // console.log(data);
    $.ajax({
        url:"/admin/permission/edit-permission",
        type:'POST',
        data:data,
        dataType:'html'
    }).done(function (data) {
        $("#editper").trigger('reset');
        $('#modaleditper').modal('hide');
        $(".content-table").html(data);
    }).fail(function (error) {
        console.log(error);
    });
}

//del-per
$(document).on('click','.btn-del-per',function (e) {
    e.preventDefault();
    var check=confirm("Bạn có muốn xóa không?");
    if(check){
        var id=$(this).val();
        $.ajax({
            url:"/admin/permission/del-permission",
            type:'GET',
            data:{id:id},
            dataType:'html'
        }).done(function (data) {
            $(".content-table").html(data);
        }).fail(function (error) {
            console.log(error.statusText);
        });
    }
   return false;
});


function addCate() {
    var form_data = new FormData($("#formaddcate")[0]);
    $.ajax({
        url: '/admin/category/add-category',
        type: 'POST',
        data: form_data,
        dataType: 'html',
        // async: false,
        processData: false,
        contentType: false,
    }).done(function (data) {
        $("#formaddcate").trigger('reset');
        $('#myModal').modal('hide');
        $(".content-table").html(data);

        }).fail(function (data) {
        var response = JSON.parse(data.responseText);
        printErrorMsg(response.errors);
    });

}
//get update category
$(document).on('click','.button_update',function () {
    var id=$(this).val();
    $('#myModalupdate').modal("show");
    $.ajax({
        url: '/admin/category/list-category/'+id,
        type: 'GET',
        // dataType: 'json',
        // async: false,
    }).done(function (data) {
        $("#namecate").val(data.item.name);
        $("#idsp").val(data.item.id);
        if(data.item.status==1){
            option='<option value="1" selected>Ẩn</option>\n' +
                   '<option value="2">Hiện</option>'
            $("#sltstatus").html(option);
        }
        else{
            option='<option value="1">Ẩn</option>\n' +
                '<option value="2"  selected >Hiện</option>'
            $("#sltstatus").html(option);
        }
        slt='<option value="0">root</option>'
        $.each(data.cate, function (i,items) {
          if(items.id==data.item.nameroot){
              slt+='<option value="'+items.id+'" selected>'+items.name+'</option>';
          }
          else{
              slt+='<option value="'+items.id+'">'+items.name+'</option>';
          }
        });
        $("#slt_edit").html(slt);

    }).fail(function (data) {
        var response = JSON.parse(data.responseText);
        printErrorMsg(response.errors);
    });
});

// edit category

function editCate() {
    var form_data = new FormData($("#formeditcate")[0]);
    var id=$(".idsp").val();
    $.ajax({
        url: '/admin/category/edit-category/'+id,
        type: 'POST',
        data: form_data,
        // dataType: 'json',
        // async: false,
        processData: false,
        contentType: false,
    }).done(function (data) {
        $("#formeditcate").trigger('reset');
        $('#myModalupdate').modal('hide');
        page=$(".idpaginate").val();
        readCate(page);

    }).fail(function (data) {
        var response = JSON.parse(data.responseText);
        printErrorMsg(response.errors);
    });

}

// del cate
$(document).on('click','.button_del',function (e) {
    e.preventDefault();
    id=$(this).val();
    var check=confirm("Bạn có muốn xóa hay không ?");
    if(check==true){
        $.ajax({
            url: '/admin/category/del-category/'+id,
            type: 'GET',
            // data: form_data,
            // dataType: 'json',
            // async: false,
            // processData: false,
            // contentType: false,
        }).done(function (data) {
            // $("#formeditcate").trigger('reset');
            // $('#myModalupdate').modal('hide');
            alert("Xóa thành công");
            page=$(".idpaginate").val();
            readCate(page);

        }).fail(function (data) {
            var response = JSON.parse(data.responseText);
            console.log(response.errors);
        });
    }
    return false;
});



// ADD PROJECT

function addProject() {
    var formdata=new FormData($("#addpro")[0]);
    $.ajax({
        url:"/admin/project/add-project",
        type:"POST",
        data:formdata,
        processData: false,
        contentType: false,
        dataType:'html'
    }).done(function (data) {
        $("#addpro").trigger('reset');
        $('#modaladdproduct').modal('hide');
        $(".content-table").html(data);
        console.log('success');
    }).fail(function (erro) {
        console.log(erro.statusText);
    });
    
}


$(document).on('click','.button_del_product',function (e) {
    e.preventDefault();
    var id=$(this).val();
    // alert(id);

    $.ajax({
        url:"/admin/project/del-project",
        type:"GET",
        data:{id:id},
        dataType:'html'
    }).done(function (data) {
        $(".content-table").html(data);
        console.log('success');
    }).fail(function (erro) {
        console.log(erro.statusText);
    });
});


$(document).on('click','.button_update_product',function (e) {
   e.preventDefault();
   var id=$(this).val();
    $("#modal_update_pro").modal('show');
    $.ajax({
        url:"/admin/project/edit-project",
        type:"GET",
        data:{id:id},
        dataType:'html'
    }).done(function (data) {
        if(data){
            $("#idpro_").val(id);
            $("#gmap").val(data.gmap);
            if(data.status==1){
                status=' <option value="1" selected>Hoạt động</option>\n' +
                    '                <option value="2">Không hoạt động</option>';
            }
            else{
                status=' <option value="1">Không Hoạt động</option>\n' +
                    '                <option value="2" selected>Không hoạt động</option>'
            }
            $("#status").html(status);

            if(data.pending==1){
                pending='<option value="1" selected>Đã thanh toán</option>\n' +
                    '                                            <option value="0">Chưa thanh toán</option>';
            }
            else{
                pending='<option value="1">Đã thanh toán</option>\n' +
                    '                                            <option value="0"  selected>Chưa thanh toán</option>';
            }
            $("#pay").html(pending);

        }
    }).fail(function (erro) {
        console.log(erro.statusText);
    });
});


$(document).on('click','.btneditpro',function (e) {
    e.preventDefault();
    var data=$("#editproject").serialize();
    $.ajax({
        url:"/admin/project/edit-project",
        type:'POST',
        data:data,
        dataType:'html'
    }).done(function (data) {
        $("#editproject").trigger('reset');
        $('#modaladdaddress').modal('hide');
        $(".content-table").html(data);
    }).fail(function (error) {
        console.log(error);
    });
});
//add address

function addaddress() {
    var data=$("#formaddaddress").serialize();
    // console.log(data);
    $.ajax({
        url:"/admin/address/add-address",
        type:'POST',
        data:data,
        dataType:'html'
    }).done(function (data) {
        $("#formaddaddress").trigger('reset');
        $('#modal_update_pro').modal('hide');
        $(".content-table").html(data);
    }).fail(function (error) {
        console.log(error);
    });
}



function printErrorMsg(msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display', 'block');
    $.each(msg, function(key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}

$(".pangin_cate").on('click','.pagination a',function (e) {
    e.preventDefault();
   var page=$(this).attr('href').split('page=')[1];
   var url="/admin/category/list-categoryp?page=";
   $(".idpaginate").val(page);
    readCate(url,page);
});

function readCate(url,page) {
    $.ajax({
       url:url+page,
    }).done(function (data) {
        $(".content-table").html(data);
    });
}
$(document).on('change','#dateend',function (e) {
    e.preventDefault();
    var datestart=new Date($("#datestart").val())
    var dateend=new Date($(this).val());
    if(datestart.getMonth()<dateend.getMonth()){
        var lastDaystart = new Date(datestart.getFullYear(),datestart.getMonth() + 1, 0);
        var beginDayend=new Date(dateend.getFullYear(), dateend.getMonth(), 1);
        if(dateend.getDate()==beginDayend.getDate()){
            soday=(lastDaystart.getDate()-datestart.getDate())+1;
            thanhtien=soday*10000;
            $("#thanhtien").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        }
        else {
            soday=(lastDaystart.getDate()-datestart.getDate())+(dateend.getDate()-beginDayend.getDate());
            thanhtien=soday*10000;
            $("#thanhtien").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        }

    }
    else if(datestart.getMonth()==dateend.getMonth()){
        soday=dateend.getDate()-datestart.getDate();
        thanhtien=soday*10000;
        $("#thanhtien").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    }
    else{
        alert("bạn bịp tôi sao");

        return false;
    }

});