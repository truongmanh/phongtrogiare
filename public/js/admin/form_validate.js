// ADD USERS
$("#addadmin").validate({
//specify the validation rules
    rules: {
        name: "required",
        role:"required",
        status: "required",
        email: {
            required: true,
            email: true //email is required AND must be in the form of a valid email address
        },
        password: {
            required: true,
            minlength: 6
        },
        // txtimage:"required",
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        role: "Bạn chưa chọn quyền",
        status:"Trạng thái không được để trống",
        password: {
            required: "Mật khẩu không được để trống",
            minlength: "Mật khẩu quá dài"
        },
        email:{
            required:"Email không được để trống",
            email: "Email không đúng định dạng"
        },

    },

    submitHandler: function(form){
        addAdmin();
    }

});

//edit user
$("#editadmin").validate({
//specify the validation rules
    rules: {
        name: "required",
        role:"required",
        status: "required",
        email: {
            required: true,
            email: true //email is required AND must be in the form of a valid email address
        },
        // txtimage:"required",
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        role: "Bạn chưa chọn quyền",
        status:"Trạng thái không được để trống",
        email:{
            required:"Email không được để trống",
            email: "Email không đúng định dạng"
        },

    },

    submitHandler: function(form){
        editAdmin();
    }

});

//ADD ROLE
$("#formaddrole").validate({
//specify the validation rules
    rules: {
        name: "required",
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
    },

    submitHandler: function(form){
        addrole();
    }

});

//edit role

$("#formeditrole").validate({
//specify the validation rules
    rules: {
        name: "required",
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
    },

    submitHandler: function(form){
        editrole();
    }

});

//addpermission

$("#addper").validate({
    //specify the validation rules
    rules: {
        name: "required",
        name_for:"required"
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        name_for: "mô tả không được để trống",
    },

    submitHandler: function(form){
        addpermission();
        // form.submit();
    }
});

//editpermisson
$("#editpermis").validate({
    //specify the validation rules
    rules: {
        name: "required",
        name_for:"required"
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        name_for: "mô tả không được để trống",
    },

    submitHandler: function(form){
        editpermission();
        // form.submit();
    }
});

//ADD category
$("#formaddcate").validate({
//specify the validation rules
    rules: {
        name: "required",
        sltname: "required",
        status: "required",
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        sltname:"Danh mục cha không được để trống",
        status:"Trạng thái không được để trống",
    },
    submitHandler: function(form){
        addCate();
    }

});

//edit category

$("#formeditcate").validate({
//specify the validation rules
    rules: {
        name: "required",
        sltname: "required",
        status: "required",
    },

//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        sltname:"Danh mục cha không được để trống",
        status:"Trạng thái không được để trống",
    },
    submitHandler: function(form){
        editCate();
    }

});

//add project

$("#addpro").validate({
//specify the validation rules
    rules: {
        name: "required",
        mota: "required",
        // sltcate: "required",
        // hoten:"required",
        // sdt:"required",
        // // thoigian:"required",
        // gia:{
        //     required:true,
        //     number:true
        // },
        // dientich: {
        //     required:true,
        //     number:true
        // },
        // imageproduct: {
        //     required: true,
        // },
        // imageicon:{
        //     required: true,
        // },
        // status: "required",
    },
//specify validation error messages
    messages: {
        name: "Tên không được để trống",
        mota: "Mô tả không được để trống",
        // sltcate:"Danh mục cha không được để trống",
        // dientich:{
        //     required:"Diện tích không được để trống",
        //     number:"Diện tích phải là số"
        // },
        // imageproduct: {
        //     required: "Ảnh không được để trống",
        //
        // },
        // imageicon:{
        //     required: "icon không được để trống",
        //
        // },
        // status:"Trạng thái không được để trống",
        // gia:{
        //     required:"Giá không được để trống",
        //     number:"Giá phải là số"
        // },
        // hoten:"Họ tên không dược để trống",
        // sdt:"Không được để trống",
        // thoigian:"Không được để trống",
    },
    submitHandler: function(form){
        addProject();
    }

});

// validate add address

$("#formaddaddress").validate({
//specify the validation rules
    rules: {
        name: "required",
        status: "required",
    },

//specify validation error messages
    messages: {
        name: "Địa điểm không được để trống",
        status:"Trạng thái không được để trống",
    },
    submitHandler: function(form){
        addaddress();
    }

});
