<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'/','uses'=>'HomeController@index']);

Route::get('/detail',['as'=>'getDetail','uses'=>'HomeController@getDetail']);
Route::get('/datatype',['as'=>'getData','uses'=>'HomeController@getType']);
Route::get('/data_type',['as'=>'get_Data','uses'=>'HomeController@getDataType']);
Route::get('/dang-bai',['as'=>'getPost','uses'=>'HomeController@getPost']);
Route::post('/dang-bai-cus',['as'=>'getPostCus','uses'=>'HomeController@addCusProduct']);





Route::get('/admin/login',["as"=>"login","uses"=>"AdminController@getLogin"]);
Route::post('/admin/login',["as"=>"postLogin","uses"=>"AdminController@postLogin"]);
Route::get('/admin/logout',["as"=>"getLogout","uses"=>"AdminController@getLogout"]);

Route::group(['middleware'=>'auth:admin'],function (){
    Route::prefix('admin')->group(function () {
        Route::get('/dasboard',["as"=>"getDasboard","uses"=>"AdminController@index"]);

        Route::prefix('/user')->group(function (){
            Route::get('/list-admin',["as"=>"getListUser","uses"=>"AdminController@getListView"]);
            Route::post('/add-admin',["as"=>"addAddAdmin","uses"=>"AdminController@addAddAdmin"]);
            Route::get('/edit-admin',["as"=>"getEditAdmin","uses"=>"AdminController@getEditAdmin"]);
            Route::post('/edit-admin',["as"=>"postEditAdmin","uses"=>"AdminController@postEditAdmin"]);
            Route::get('/del-admin',["as"=>"getDelAdmin","uses"=>"AdminController@getDelAdmin"]);

        });
        Route::prefix('/role')->group(function (){
            Route::get('/list-role',["as"=>"getListRole","uses"=>"RoleController@index"]);
            Route::post('/add-role',["as"=>"getAddRole","uses"=>"RoleController@store"]);
            Route::get('/edit-role',["as"=>"getEditRole","uses"=>"RoleController@edit"]);
            Route::post('/edit-role',["as"=>"postEditRole","uses"=>"RoleController@update"]);
            Route::get('/del-role',["as"=>"getDelRole","uses"=>"RoleController@delete"]);
        });

        Route::prefix('/permission')->group(function (){
            Route::get('/list-permission',["as"=>"getListPermission","uses"=>"PermissionController@index"]);
            Route::post('/add-permission',["as"=>"getAddPermission","uses"=>"PermissionController@store"]);
            Route::get('/edit-permission',["as"=>"getAddPermission","uses"=>"PermissionController@edit"]);
            Route::post('/edit-permission',["as"=>"postAddPermission","uses"=>"PermissionController@update"]);
            Route::get('/del-permission',["as"=>"getDelPermission","uses"=>"PermissionController@delete"]);
        });
        Route::prefix('/category')->group(function (){
            Route::get('/list-category/{id}',["as"=>"getListCateid","uses"=>"CategoryController@show"]);
            Route::get('/list-category',["as"=>"getListCate","uses"=>"CategoryController@index"]);
            Route::get('/list-categoryp',["as"=>"getListCatep","uses"=>"CategoryController@getCate"]);
            Route::post('/add-category',["as"=>"getAddCate","uses"=>"CategoryController@store"]);
            Route::post('/edit-category/{id}',["as"=>"getEditCate","uses"=>"CategoryController@update"]);
            Route::get('/del-category/{id}',["as"=>"getDelCate","uses"=>"CategoryController@destroy"]);
        });

        Route::prefix('/project')->group(function (){
            Route::get('/list-project',["as"=>"getListPro","uses"=>"ProductController@index"]);
            Route::post('/add-project',["as"=>"getAddPro","uses"=>"ProductController@store"]);
            Route::post('/edit-project',["as"=>"postEditPro","uses"=>"ProductController@update"]);
            Route::get('/edit-project',["as"=>"getEditPro","uses"=>"ProductController@edit"]);
            Route::get('/del-project',["as"=>"getDelPro","uses"=>"ProductController@destroy"]);
        });

        Route::prefix('/address')->group(function (){
            Route::get('/list-address',["as"=>"getListAddress","uses"=>"AddressController@index"]);
            Route::post('/add-address',["as"=>"getAddAddress","uses"=>"AddressController@store"]);
            Route::post('/edit-address',["as"=>"getEditAddress","uses"=>"AddressController@update"]);
            Route::get('/del-address',["as"=>"getDelAddress","uses"=>"AddressController@delete"]);
        });
    });
});

