<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <div class="navbar-collapse collapse" id="navigation">

            <ul class="nav navbar-nav navbar-left">
                <li class="active"><a href="/">Home</a>
                </li>
                <li>
                    <a href="dang-bai" class="dangbai">Đăng bài</a>
                </li>
            </ul>

        </div>
        <!--/.nav-collapse -->

        <div class="navbar-buttons">

        </div>

        <div class="collapse clearfix" id="search">

        </div>
        <!--/.nav-collapse -->

    </div>
    <!-- /.container -->
</div>
<!-- /#navbar -->

<!-- *** NAVBAR END *** -->
<div class="container">
    <div class="col-md-12">
        <div id="main-slider">
            <div class="item">
                <img src="images/products/1.jpg" alt="" class="img-responsive">
            </div>
            <div class="item">
                <img class="img-responsive" src="images/products/2.jpg" alt="">
            </div>
            <div class="item">
                <img class="img-responsive" src="images/products/3.jpg" alt="">
            </div>
            {{--<div class="item">--}}
                {{--<img class="img-responsive" src="site/img/main-slider4.jpg" alt="">--}}
            {{--</div>--}}
        </div>
        <!-- /#main-slider -->
    </div>
</div>
