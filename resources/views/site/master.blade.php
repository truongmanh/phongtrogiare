<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{asset('')}}">
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        @yield('title')
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="site/css/font-awesome.css" rel="stylesheet">
    <link href="site/css/bootstrap.min.css" rel="stylesheet">
    <link href="site/css/animate.min.css" rel="stylesheet">
    <link href="site/css/owl.carousel.css" rel="stylesheet">
    <link href="site/css/owl.theme.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css" rel="stylesheet">
    <!-- theme stylesheet -->
    <link href="site/css/style.default.css?v=<?time()?>" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="site/css/custom.css?v=<?time()?>" rel="stylesheet">

    <script src="site/js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">

</head>

<body>

@include('site.header')
{{--<div id="all">--}}

    <!-- /#content -->
   @yield('content')


  @include('site.footer')
    <!-- *** COPYRIGHT END *** -->



{{--</div>--}}

<script src="site/js/jquery-1.11.0.min.js"></script>
<script src="site/js/bootstrap.min.js"></script>
<script src="site/js/jquery.cookie.js"></script>
<script src="site/js/waypoints.min.js"></script>
<script src="site/js/modernizr.js"></script>
{{--<script src="admin/assest/bower_components/jquery/dist/jquery.min.js"></script>--}}
<!-- jQuery UI 1.11.4 -->
<script src="admin/assest/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script src="site/js/bootstrap-hover-dropdown.js"></script>
<script src="site/js/owl.carousel.min.js"></script>
<script src="site/js/front.js"></script>
<script src="site/js/main.js?v=<?= time()?>"></script>
<script src="site/js/tinymce/tinymce.min.js"></script>

</body>

</html>