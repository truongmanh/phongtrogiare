@extends('site.master')
@section('title','trang chủ')
@section('content')

    <div id="content">
        {{--<div id="all">--}}
        <div id="hot">

            <div class="box">
                <div class="container">
                    <div class="col-md-12">
                        <h2>Phòng trọ mới nhất</h2>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="product-slider">
                @foreach($pro as $item)
                    <div class="item">
                        <div class="product product_item" pro_id="{{$item->id}}">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                     
                                            <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon" >
                                        
                                    </div>
                                    <div class="back">
                                       
                                            <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">
                                       
                                    </div>
                                </div>
                            </div>
                            <a href="detail.html" class="invisible">
                                <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">
                            </a>
                            <div class="text">
                                <h3><a href="detail.html">Khu vực:{{$item->address->name}}</a></h3>
                            </div>
                            <!-- /.text -->
                        </div>
                        <!-- /.product -->
                    </div>
                    @endforeach
                </div>
                <!-- /.product-slider -->
            </div>
            <!-- /.container -->

        </div>
        <div class="container">
            <div class="box info-bar">
                <div class="row">
                    <div class="col-sm-12 col-md-4 products-showing">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a>
                            </li>
                            <li><a href="#" class="label_cate" cate_label="0">Danh mục</a>
                            </li>
                        
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <!-- *** MENUS AND FILTERS ***
_________________________________________________________ -->
                <div class="panel panel-default sidebar-menu">


                    <div class="panel-body">
                        <button class="btn btn-success"> <h3 class="panel-title">Danh mục</h3></button>
                        <hr />
                        <ul class="nav nav-pills nav-stacked category-menu">
                        @foreach($cate as $item)
                            <li class="cate_product" cate_id="{{$item->id}}" cate_name="{{$item->name}}">
                                + {{$item->name}}
                            </li>
                        @endforeach
                        </ul>

                    </div>
                </div>

                <div class="panel panel-default sidebar-menu">

                    <div class="panel-body">

                        <form id="formkv" method="post">
                            <button class="btn btn-warning"><h3 class="panel-title">Khu vực</h3></button>
                            <hr />
                            <div class="form-group">
                                @foreach($address as $item)
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="address[]" value="{{$item->id}}">{{$item->name}}
                                    </label>
                                </div>
                                @endforeach
                            </div>
                            <hr />
                            <button class="btn btn-primary"><h3 class="panel-title">Diện tích</h3></button>
                            <hr />
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="dientich[]" value="14"> <span class="colour white"></span> 14 m<sup>2</sup>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"  name="dientich[]" value="15"> <span class="colour blue"></span> 15 m<sup>2</sup>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"  name="dientich[]" value="16"> <span class="colour green"></span> 16 m<sup>2</sup>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"  name="dientich[]" value="18"> <span class="colour yellow"></span> 18 m<sup>2</sup>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"  name="dientich[]" value="20"> <span class="colour red"></span> 20 m<sup>2</sup>
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" name="cate_id" value="" class="cate_id">
                        </form>

                    </div>
                </div>
                <!-- *** MENUS AND FILTERS END *** -->
            </div>

            <div class="col-md-9">

                <div class="row products">
                    @foreach($pro as $item)
                    <div class="col-md-4 col-sm-4 product_item" pro_id="{{$item->id}}">
                        <div class="product">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                            <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">

                                    </div>
                                    <div class="back">
                                            <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">
                                    </div>
                                </div>
                            </div>
                            <a href="" class="invisible">
                                <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">
                            </a>
                            <div class="text">
                                <h4>{{$item->name}}</h4>
                                <small>Khu vực:{{$item->address->name}}</small>
                            </div>
                            <!-- /.text -->
                        </div>
                        <!-- /.product -->
                    </div>
                    @endforeach
                </div>

                <div class="content">
                </div>
                <div class="customerpost">

                </div>
            
            </div>
            <!-- /.col-md-9 -->
        </div>
        <!-- /.container -->
    {{--</div>--}}
    <!-- /#content -->
    </div>
    @endsection