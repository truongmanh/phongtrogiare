@foreach($pro as $item)
    <div class="col-md-4 col-sm-4 product_item" pro_id="{{$item->id}}">
        <div class="product">
            <div class="flip-container">
                <div class="flipper">
                    <div class="front">
                        <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">

                    </div>
                    <div class="back">
                        <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">
                    </div>
                </div>
            </div>
            <a href="" class="invisible">
                <img src="/images/icons/{{$item->images}}" alt="" class="img-responsive image_icon">
            </a>
            <div class="text">
                <h4>{{$item->name}}</h4>
                <small>Khu vực:{{$item->address->name}}</small>
            </div>
            <!-- /.text -->
        </div>
        <!-- /.product -->
    </div>
@endforeach