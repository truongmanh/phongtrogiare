<div class="content-detail">
 <div class="row" id="productMain">
        <div class="col-md-12">
             <button class="btn btn-warning btn-xoa">Xóa</button>
        </div>
                    <div class="col-sm-6">
                        <div id="mainImage">
                            <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive image_product">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box">
                            <h1 class="text-center">{{$pro->name}}</h1>
                            <h3>Khu vực:{{$pro->address->name}}</h3>
                            <p>Địa chỉ:{{$pro->address->name}}</p>
                            <p>Giá:{{number_format($pro->price, 3)}} vnđ</p>
                            <p>Diện tích:{{$pro->dientich}} m<sup>2</sup></p>
                           
                           <h1>Thông tin liên hệ:</h1>
                          <p> Chủ phòng:{{$pro->name_own}}</p>
                            <p> Số điện thoại:{{$pro->phone}}</p>
                        </div>

                        <div class="row" id="thumbs">
                            <div class="col-xs-4">
                                <a href="/images/icons/{{$pro->images}}" class="thumb">
                                    <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="/images/icons/{{$pro->images}}" class="thumb">
                                    <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="/images/icons/{{$pro->images}}" class="thumb">
                                    <img src="/images/icons/{{$pro->images}}" alt="" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="box" id="details">
                    <p>
                    <h4>Chi tiết</h4>
                    <p>{!!$pro->mota !!}</p>

                    <hr>
                    <div class="social">
                        <h4>Show it to your friends</h4>
                        <p>
                            <a href="http://www.facebook.com/" target="_blank" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                        </p>
                    </div>
                    <div class="map">
                        <iframe src="{{$pro->gmap}}" width="800" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
             </div>

</div>
