<div id="dang-bai">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>Chuyên mục cho thuê</h1>
        </div>
        <div class="panel-body">
                <form  action="" method="post" enctype="multipart/form-data" id="cuspostform">
                    {{csrf_field()}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Tên dự án</label>
                            <input type="text" class="form-control"   placeholder="Tên dự án" name="name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Danh mục</label>
                            <select name="sltcate" class="form-control">
                                <option value="">--chọn--</option>
                                @foreach($cate as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Diện tích</label>
                            <input type="text" class="form-control" name="dientich" placeholder="Diện tích">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Giá</label>
                            <input type="text" class="form-control" name="gia" placeholder="Giá">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Số điện thoại</label>
                            <input type="text" class="form-control" name="sdt" placeholder="sdt">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Họ tên chủ nhà</label>
                            <input type="text" class="form-control" name="name_own" placeholder="tên">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Bắt đầu</label>
                            <input type="date" class="form-control" name="starttime" id="datestart1" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Kết thúc</label>
                            <input type="date" class="form-control" name="endtime" id="dateend1" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Khu vực</label>
                            <select name="diadiem"   class="form-control">
                                <option value="" >--chọn--</option>
                                @foreach($address as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Địa chỉ</label>
                            <input type="text" class="form-control" name="address_detail">
                        </div>
                    </div>

                        <div class="form-group">
                               <label >Thành tiền(Đv:vnđ)</label>
                               <input type="text" class="form-control" id="thanhtien" name="thanhtien" value="0" disabled>
                               <input type="hidden" class="form-control" id="thanhtien1" name="thanhtien1">
                        </div>

                    <div class="clear-fix"></div>
                        <div class="form-group">
                            <label >Icon</label>
                            <input type="file" class="form-control" name="imageicon" id="imageicon1">
                        </div>
                        <div class="preview-icon1">

                        </div>


                    <div class="form-group">
                        <label >Image</label>
                        <input type="file" class="form-control" name="imageproduct[]" id="imageproduct1" multiple>
                    </div>
                    <div class="preview-image1">

                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label>
                        <textarea  class="form-control" name="mota"></textarea>
                    </div>
                    <!-- /.box-body -->
                    <button type="submit" class="btn btn-primary" >Tiếp tục</button>
                </form>
        </div>
    </div>
</div>
<div class="thanhtoan">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">Bạn vui lòng chuyển khoản vào tài khoản vcb này:</div>
            <div class="panel-body">
                <ul>
                    <li>Tên tài khoản:Trương Văn Mạnh</li>
                    <li>Số tài khoản:09642428237</li>
                    <li>Chi nhanh:vietcombank vĩnh phúc</li>
                </ul>
                <button class="btn btn-warning btn-success">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="thanks">
    <h3>Bài viết của bạn sẽ dược duyệt trong thời gian sớm nhất.Cảm ơn !</h3>
</div>
<script>
    tinymce.init({
        selector:'textarea',
        plugins: ['lists','link'],
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
</script>
<script type="text/javascript">
    $("#imageproduct1").on('change',function (e) {
        var files=e.target.files;
        filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
            var f = files[i]
            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
                var file = e.target;
                var temp='<span class="list"><img src="'+e.target.result+'" alt="" width="80px"><span class="remove"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></span></span>';
                $(".preview-image1").append(temp);

                $(".remove").click(function(){
                    $(this).parent(".list").remove();
                });

            });
            fileReader.readAsDataURL(f);
        }
    });
    $.validator.addMethod("money", function(value, element) {
            var isValidMoney = /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Lương không đúng định dạng (0 or 0.000)"
    );
    $("#cuspostform").validate({
//specify the validation rules
        rules: {
            name: "required",
            starttime:"required",
            endtime:"required",
            mota: "required",
            sltcate: "required",
            diadiem: "required",
            address_detail: "required",
            name_own:"required",
            sdt: {
                required:true,
                number:true,
                minlength:6,
                maxlength:11
            },
            // thoigian:"required",
            gia:{
                required:true,
                money:true,
                minlength:3,
                maxlength:4
            },
            dientich: {
                required:true,
                number:true
            },
            imageproduct: {
                required: true,
            },
            imageicon:{
                required: true,
            },
        },
//specify validation error messages
        messages: {
            name: "Tên không được để trống",
            starttime: "Thời gian bắt đầu không được để trống",
            endtime: "Thời gian kết thúc không được để trống",
            diadiem: "Khu vực không được để trống",
            address_detail: "Địa chỉ không được để trống",
            name_own: "Tên chủ nhà không được để trống",
            mota: "Mô tả không được để trống",
            sltcate:"Danh mục cha không được để trống",

            sdt:{
                required:"Số điện thoại không được để trống",
                number:"Số điện thoại phải là số",
                minlength:"Số điện thoại quá ngắn",
                maxlength:"Số điện thoại quá dài"
            },
            gia:{
                required:"giá không được để trống",
                money:"giá không đúng định dạng",
                minlength:"giá quá ngắn",
                maxlength:"giá quá dài"
            },
            dientich:{
                required:"Diện tích không được để trống",
                number:"Diện tích phải là số"
            },
            imageproduct: {
                required: "Ảnh không được để trống",

            },
            imageicon:{
                required: "icon không được để trống",

            },
        },
        submitHandler: function(form){
                var form_data = new FormData($("#cuspost")[0]);
    $.ajax({
        url: '/dang-bai-cus',
        type: 'post',
        data: form_data,
        dataType: 'json',
        // async: false,
        processData: false,
        contentType: false,
    }).done(function (data) {
        console.log(data.message);
        if(data.message){
            $(".thanhtoan").show();
            $("#dang-bai").hide();
        }
    }).fail(function (data) {
        console.log(data);
        // var response = JSON.parse(data.responseText);
        // printErrorMsg(response.errors);
    });
        }
    });


</script>