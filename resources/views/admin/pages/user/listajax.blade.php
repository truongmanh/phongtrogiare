<div class="box-body">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Stt</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Edit</th>
            <th>Del</th>
        </tr>
        <?php $stt=1?>
        @foreach($user as $item)
            <tr>
                <th>{{$stt++}}</th>
                <th>{{$item->name}}</th>
                <th>{{$item->email}}</th>
                <th>@if($item->role==1){{"admin"}}
                    @elseif($item->role==2){{"staff"}}
                    @else{{"user"}}
                    @endif
                </th>
                <th><button class="btn btn-warning btn-edit-admin"  value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                <th><button class="btn btn-danger btn-del-admin"  value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>