@extends('admin.master')
@section('title',"Quản lí user")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Quản lí user</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách users</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách users</h3>
                        </div>
                        <button type="button" class="btn btn-info btn-success pull-right" id="button_admin" data-toggle="modal" style="margin: 5px 0px">Thêm users</button>
                        <!-- /.box-header -->
                       <div class="content-table">
                           <div class="box-body">
                               <table class="table table-bordered">
                                   <tbody>
                                   <tr>
                                       <th>Stt</th>
                                       <th>Name</th>
                                       <th>Email</th>
                                       <th>Role</th>
                                       <th>Edit</th>
                                       <th>Del</th>
                                   </tr>
                                   <?php $stt=1?>
                                   @foreach($user as $item)
                                       <tr>
                                           <th>{{$stt++}}</th>
                                           <th>{{$item->name}}</th>
                                           <th>{{$item->email}}</th>
                                           <th>@if($item->role==1){{"admin"}}
                                               @elseif($item->role==2){{"staff"}}
                                               @else
                                                   {{"Publicer"}}

                                               @endif
                                           </th>
                                           <th><button class="btn btn-warning btn-edit-admin" value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                                           <th><button class="btn btn-danger btn-del-admin" value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
                                       </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li><a href="#">«</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm users</h4>
                        </div>
                        <div class="modal-body">
                            <form  action=""  method="POST" id="addadmin" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="">User name</label>
                                        <input type="text" class="form-control"   placeholder="Nhập Username" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email address</label>
                                        <input type="email" class="form-control" name="email" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Role</label>
                                        <select name="role" class="form-control">
                                            <option value="">--chọn--</option>
                                            <option value="1">admin</option>
                                            <option value="2">staff</option>
                                            <option value="3">publicer</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Trạng thái</label>
                                        <select name="status"   class="form-control">
                                            <option value="" >--chọn--</option>
                                            <option value="1">Hoạt động</option>
                                            <option value="2">Không hoạt động</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Image</label>
                                        <input type="file" name="txtimage">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            {{--modal sửa--}}

            <div class="modal fade" id="modaleditadmin" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa admin</h4>
                        </div>
                        <div class="modal-body">
                            <form  action=""  method="POST" id="editadmin" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="">User name</label>
                                        <input type="text" class="form-control" id="name"  placeholder="Nhập Username" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email address</label>
                                        <input type="email" class="form-control" id="txtemail" name="email" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="id" id="ideditadmin">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Role</label>
                                        <div id="role">
                                            <select name="role" class="form-control">
                                                <option value="1">admin</option>
                                                <option value="2">staff</option>
                                                <option value="3">publicer</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="">Trạng thái</label>
                                       <div id="status">
                                           <select name="status"   class="form-control">
                                               <option value="" >--chọn--</option>
                                               <option value="1">Hoạt động</option>
                                               <option value="2">Không hoạt động</option>
                                           </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Image</label>
                                        <input type="file" name="txtimage">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary btneditadmin" disabled >Sửa</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection()