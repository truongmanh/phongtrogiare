@extends('admin.master')
@section('title',"Quản lí role")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Quản lí role</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách role</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách role</h3>
                        </div>
                        <button type="button" class="btn btn-info btn-success pull-right" data-toggle="modal" data-target="#myModal" id="addrole" style="margin: 5px 0px">Thêm role</button>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <div class="content-table">
                              <table class="table table-bordered">
                                  <tbody>
                                  <tr>
                                      <th>Stt</th>
                                      <th>Name</th>
                                      <th>Edit</th>
                                      <th>Del</th>
                                  </tr>
                                  <?php $stt=1?>
                                  @foreach($role as $item)
                                      <tr>
                                          <th>{{$stt++}}</th>
                                          <th>{{$item->name}}</th>
                                          <th><button class="btn btn-warning btn-edit-role" value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                                          <th><button class="btn btn-danger btn-del-role" value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
                                      </tr>
                                  @endforeach
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}

            <div class="modal fade" id="modaladdrole" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm role</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="formaddrole">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="">Name role</label>
                                        <input type="text" class="form-control"   placeholder="Nhập role" name="name">
                                    </div>

                                    <div class="checkbox" >

                                        <div class="col-md-3">
                                            <p>Category</p>
                                            @foreach($per as $item)
                                                @if($item->permission_for=="cate")
                                            <label><input type="checkbox" value="{{$item->id}}" name="per_role[]">{{$item->name}}</label>
                                                @endif
                                            @endforeach
                                        </div>

                                        <div class="col-md-3">
                                            <p>User</p>
                                            @foreach($per as $item)
                                                @if($item->permission_for=="user")
                                                    <label><input type="checkbox" value="{{$item->id}}" name="per_role[]">{{$item->name}}</label>
                                                @endif
                                            @endforeach
                                        </div>
                                        <div class="col-md-3">
                                            <p>Product</p>
                                            @foreach($per as $item)
                                                @if($item->permission_for=="product")
                                                    <label><input type="checkbox" value="{{$item->id}}" name="per_role[]">{{$item->name}}</label>
                                                @endif
                                            @endforeach
                                        </div>

                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            {{--modal sua--}}

            <div class="modal fade" id="modaleditrole" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa role</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="formeditrole">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="">Name role</label>
                                        <input type="text" class="form-control" id="name"  placeholder="Nhập role" name="name">
                                    </div>
                                    <input type="hidden" class="form-control" id="idrole"  placeholder="Nhập role" name="id">
                                    <div class="checkbox" id="checkbox">
                                </div>


                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary btneditrole" disabled >Sửa</button>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection()