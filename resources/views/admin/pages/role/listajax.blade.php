<table class="table table-bordered">
    <tbody>
    <tr>
        <th>Stt</th>
        <th>Name</th>
        <th>Edit</th>
        <th>Del</th>
    </tr>
    <?php $stt=1?>
    @foreach($role as $item)
        <tr>
            <th>{{$stt++}}</th>
            <th>{{$item->name}}</th>
            <th><button class="btn btn-warning btn-edit-role" value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
            <th><button class="btn btn-danger btn-del-role" value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
        </tr>
    @endforeach
    </tbody>
</table>