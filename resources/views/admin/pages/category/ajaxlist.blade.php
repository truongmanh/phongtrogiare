<div class="box-body">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Stt</th>
            <th>Tên</th>
            <th>Trạng thái</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        <?php $stt=1?>
        @foreach($cate as $item)
            <tr>
                <th>{{$stt++}}</th>
                <th>{{$item->name}}</th>
                <th>@if($item->status==2){{"Hiện"}}
                    @else($item->status==1){{"Ẩn"}}
                    @endif
                </th>
                <th><button type="button" class="button_update" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                <th><button type="button" class="button_del" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<!-- /.box-body -->
<div class="box-footer clearfix">
    {{$cate->links()}}
</div>