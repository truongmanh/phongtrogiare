<?php
function Menu_mutil($data,$parent=0,$str="--|",$select=0){
    foreach ($data as $item){
        $id=$item['id'];
        $name=$item['name'];
        $parent_id=$item['nameroot'];
        if($parent_id==$parent){
            if($select!=0 && $id==$select) {
                echo "<option value='" . $id . "' selected='selected'>" . $str . " " . $name . "</option>";
            }
            else{
                echo "<option value='" . $id . "'>" . $str . " " . $name . "</option>";
            }
            Menu_mutil($data,$id,$str."--|");
        }
    }
}