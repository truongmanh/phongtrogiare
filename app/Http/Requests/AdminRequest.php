<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
           'name'=>'required|unique:Admin,name',
            'email'=>'required|unique:Admin,email',
            'role'=>'required',
            'status'=>'required',
            'password'=>'required'
        ];
    }


    public function messages()
    {
      return [
          "name.required" =>"Tên không được để trống",
          "name.unique"=>"Tên không được để trùng",
          "email.required"=>"email không được để trống",
          "email.unique"=>"email không được trùng",
          "role.required"=>"role không được để trống",
          "password.required"=>"Password không được để trống",
          "status.required"=>"status không được để trống"
      ];
    }
}
