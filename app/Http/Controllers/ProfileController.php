<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function addProfile(Request $req){

        try {
            $pro=new Profile();
            $pro->name=$req->input('fullname');
            $pro->images=$req->input('image');
            $pro->age=$req->input('age');
            if($pro->save()){
            $response=['responsive'=>'add success','success'=>true];
            return $response;}
        } catch(\Exception $ex){
           $response=['responsive'=>'add not success','success'=>false];
            return $response;
        }

    }
}
