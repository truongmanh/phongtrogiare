<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\image_product;
use App\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cate=Category::all();
        $address=Address::all();
        $pro=Product::orderBy('id','desc')->paginate(10);
        return view('admin.pages.product.list',compact('cate','pro','address'));
    }
public function getView(){
    $pro=Product::orderBy('id','desc')->paginate(10);
    return view('admin.pages.product.ajaxlist',compact('pro'));
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     try{
         $product= new Product();
         $product->name=$request->name;
         $product->alias=str_slug($request->name)."html";
         $product->cate_id=$request->sltcate;
         $product->price=$request->gia;
         $product->name_own=$request->name_own;
         $product->phone=$request->sdt;
         $product->dientich=$request->dientich;
         $product->mota=$request->mota;
         $product->address_id=$request->diadiem;
         $product->address_detail=$request->address_detail;
         $product->thanhtien=$request->thanhtien;
         $product->pending=$request->pending;
         $product->gmap=$request->gmap;
         $product->status=$request->status;
         $product->start_day=Carbon::parse($request->starttime);
         $product->end_day=Carbon::parse($request->endtime);
         if($request->hasFile('imageicon')){
             $file_icon=$request->file('imageicon');
             $name_icon=time().".".$file_icon->getClientOriginalExtension();
             $path = public_path('/images/icons/');
             $file_icon->move($path, $name_icon);
             $product->images=$name_icon;
         }
         if($product->save()){

             if($request->hasFile('imageproduct')){
                $files=$request->file('imageproduct');
                foreach ($files as $item){
                    $name=time().".".$item->getClientOriginalName();
                    $path = public_path('/images/products/');
                    $item->move($path, $name);
                    $imagep=new image_product();
                    $imagep->product_id=$product->id;
                    $imagep->image=$name;
                    $imagep->save();
                }
             }

         }
        return $this->getView();
     }
     catch (\Illuminate\Database\QueryException $ex){
         return $ex->getMessage();
     }
     catch (\Exception $e){
         return $e->getMessage();
     }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pro=Product::findOrFail($request->id);
        return response()->json($pro);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $pro=Product::findOrFail($request->id);
            $pro->gmap=$request->gmap;
            $pro->status=$request->status;
            $pro->pending=$request->pay;
            $pro->update();
            return $this->getView();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $pro=Product::findOrFail($request->id);
        $pro->delete();
        return $this->getView();
    }
}
