<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index()
    {
        $address=Address::all();
       return view('admin.pages.address.list',compact('address'));
    }

    public function getIndex()
    {
        $address=Address::all();
        return view('admin.pages.address.listajax',compact('address'));
    }

    public function store(Request $request)
    {
        try{
            $address=new Address();
            $address->name=$request->name;
            $address->status=$request->status;
            $address->save();
            return $this->getIndex();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
