<?php

namespace App\Http\Controllers;

use App\image_product;
use Carbon\Carbon;
use FarhanWazir\GoogleMaps\GMapsServiceProvider;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Address;

class HomeController extends Controller
{
    public function index(){
       try{
           $pro=Product::where('status',1)->orderBy('id','desc')->paginate(9);
           $address=Address::all();
           return view('site.pages.trangchu',compact('pro','address'));
       }
       catch (\Exception $e){
           return $e->getMessage();
       }
    }

    public function getDetail(Request $request){
        try{
            $pro=Product::findOrFail($request->id);
            return view('site.pages.chitiet',compact('pro'));
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getType(Request $request){
        try{
            $pro=Product::where('cate_id',$request->id)->get();

            return view('site.pages.loaisanpham',compact('pro'));
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getDataType(Request $request){
        try{
            if($request->cate_id !=""){
                if($request->address!="" &&$request->dientich!=""){
                    $pro=Product::where('cate_id',$request->cate_id)
                    ->whereIn('address_id',$request->address)
                    ->whereIn('dientich',$request->dientich)
                        ->get();
                }
                elseif($request->address=="" &&$request->dientich!=""){
                    $pro=Product::where('cate_id',$request->cate_id)
//                        ->whereIn('address_id',$request->address)
                        ->whereIn('dientich',$request->dientich)
                        ->get();
                }

                elseif ($request->address!="" &&$request->dientich==""){
                    $pro=Product::where('cate_id',$request->cate_id)
                        ->whereIn('address_id',$request->address)
//                ->whereIn('dientich',$request->dientich)
                        ->get();
                }
                else{
                    $pro=Product::where('cate_id',$request->cate_id)
                        ->get();
                }
            }
            else{
                if($request->address!="" &&$request->dientich!=""){
                    $pro=Product::
                        whereIn('address_id',$request->address)
                       ->whereIn('dientich',$request->dientich)
                        ->get();
                }
                elseif($request->address=="" &&$request->dientich!=""){
                    $pro=Product::
//                        ->whereIn('address_id',$request->address)
                        whereIn('dientich',$request->dientich)
                        ->get();
                }

                elseif ($request->address!="" &&$request->dientich==""){
                    $pro=Product::
                        whereIn('address_id',$request->address)
//                ->whereIn('dientich',$request->dientich)
                        ->get();
                }
                else{
                    $pro=Product::all();

                }
            }

            return view('site.pages.loaisanpham',compact('pro'));

        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getPost(){
        $cate=Category::all();
        $address=Address::all();
        return view('site.pages.customerPost',compact('cate','address'));
    }

    public function addCusProduct(Request $request){
        try{
            $product= new Product();
            $product->name=$request->name;
            $product->alias=str_slug($request->name)."html";
            $product->cate_id=$request->sltcate;
            $product->price=$request->gia;
            $product->name_own=$request->name_own;
            $product->phone=$request->sdt;
            $product->dientich=$request->dientich;
            $product->mota=$request->mota;
            $product->address_id=$request->diadiem;
            $product->address_detail=$request->address_detail;
            $product->status=2;
            $product->pending=0;
            $product->thanhtien=$request->thanhtien1;
            $product->start_day=Carbon::parse($request->starttime);
            $product->end_day=Carbon::parse($request->endtime);
            if($request->hasFile('imageicon')){
                $file_icon=$request->file('imageicon');
                $name_icon=time().".".$file_icon->getClientOriginalExtension();
                $path = public_path('/images/icons/');
                $file_icon->move($path, $name_icon);
                $product->images=$name_icon;
            }
            if($product->save()){

                if($request->hasFile('imageproduct')){
                    $files=$request->file('imageproduct');
                    foreach ($files as $item){
                        $name=time().".".$item->getClientOriginalName();
                        $path = public_path('/uploads/products/');
                        $item->move($path, $name);
                        $imagep=new image_product();
                        $imagep->product_id=$product->id;
                        $imagep->image=$name;
                        $imagep->save();

                    }
                }

            }
        return response()->json([
           'message'=>'success'
        ]);
        }
        catch (\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }

    }

}
