<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
    }

    public function index(Request $request)
    {
         $cate=Category::paginate(3);
         $data=Category::all();
         return view('admin.pages.category.list',compact('cate','data','data_show'));
    }
    public function getCate(){
        $cate=Category::paginate(3);
        $data=Category::all();
        return view("admin.pages.category.ajaxlist",compact('cate','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        try{
            $cate= new Category();
            $cate->name=$request->name;
            $cate->alias=str_slug($request->name).".html";
            $cate->parent_id=$request->sltname;
            $cate->status=$request->status;
            $cate->created_by=1;
            $file=$request->file('txticon');
            if(strlen($file)>0){
                $filename=time().'.'.$file->getClientOriginalExtension();
                $path="uploads/cates/";
                $file->move($path,$filename);
                $cate->icon=$filename;
            }
            if( $cate->save()){
                return $this->getCate();

            }
            else{
                return response()->json(['error'=>$request->errors()->all()]);
            }
        }
        catch (\Exception $e){
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data_show=Category::find($id);
       $data_cate=Category::all();
      return response()->json([
         'item'=>$data_show,
          'cate'=>$data_cate
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cate=Category::findOrFail($id);
        $cate->name=$request->name;
        $cate->alias=str_slug($request->name).".html";
        $cate->parent_id=$request->sltname;
        $cate->status=$request->status;
        $cate->created_by=1;
        $file=$request->file('txticon');
        if(strlen($file)>0){
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/cates/";
            $file->move($path,$filename);
            $cate->icon=$filename;
        }
        if( $cate->update()){
            return $this->getCate();

        }
        else{
            return response()->json(['error'=>$request->errors()->all()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cate=Category::findOrFail($id);
        $cate->delete();
    }
}
