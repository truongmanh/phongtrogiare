<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
   public function index(){

        $role=Role::all();
        $per=Permission::all();
       return view('admin.pages.role.list',compact('role','per'));
   }
   public function getRole(){
       $role=Role::all();
       return view('admin.pages.role.listajax',compact('role'));
   }

   public function store(RoleRequest $request){
        try{
            $role=new Role();
            $role->name=$request->name;
            $role->save();
            $role->Permissions()->sync($request->per_role);
            return $this->getRole();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
   }

   public function edit(Request $request){
       $role=Role::find($request->id);
       $per=Permission::all();
       $rp=$role->Permissions()->get();
        return response()->json([
            'role'=>$role,
            'rp'=>$rp,
            'per'=>$per
        ]);
   }

   public function update(RoleRequest $request){
       try{
           $role=Role::find($request->id);
           $role->name=$request->name;
           $role->update();
           $role->Permissions()->sync($request->per_role);
           return $this->getRole();
       }
       catch (\Exception $e){
           return $e->getMessage();
       }
   }

   public function delete(Request $request){
       try{
           $role=Role::find($request->id);
           $role->delete();
           return $this->getRole();
       }
       catch (\Exception $e){
           return $e->getMessage();
       }
   }
}
