<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table="roles";

    public function Permissions(){
        return $this->belongsToMany('App\Permission')->withTimestamps();;
    }
}
